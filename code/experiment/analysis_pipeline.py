# intro

# Block 1 - Getting the data
import requests
import zipfile
from glob import glob
from io import BytesIO
import os
import pandas as pd
import pingouin as pg

print('Downloading started')

url = ['https://gitlab.com/julia-pfarr/nowaschool/-/',
       'raw/main/school/materials/CI_CD/crtt.zip?ref_type=heads']

filename = url.split('/')[-1]

req = requests.get(url)
print('Downloading Completed')

zipfile = zipfile.ZipFile(BytesIO(req.content))
zipfile.extractall(
    '/mnt/c/Users/si4187/Desktop/Promotion/Workshops/NOWA_school'
    )

# Block 2 - Data conversion
data_files = glob(
    '/mnt/c/Users/si4187/Desktop/Promotion/Workshops/NOWA_school'
    )

columns_select = ['participant_id', 'age',
                  'left-handed', 'Do you like this session?',
                  'session', 'TargetImage',
                  'keyboard_response.corr', 'trialRespTimes']

for index, participant in enumerate(data_files):

    print('Working on %s, file %s/%s'
          % (participant, index+1, len(data_files)))

    data_loaded_part = pd.read_csv(participant, delimiter=',')

    data_loaded_sub_part = data_loaded_part[columns_select]

    print('Checking for nan across rows and deleting rows with only nan.')

    rows_del = []

    for index, values in data_loaded_sub_part[data_loaded_sub_part.columns[5:]].iterrows():

        if values.isnull().any():
            rows_del.append(index)

    data_loaded_sub_part = data_loaded_sub_part.drop(rows_del)

    data_loaded_sub_part.reset_index(drop=True, inplace=True)

    print('Adding trial_type column.')

    list_trial_type = 6 * ["practice"] + 12 * ["experiment"]
    + 6 * ["practice"] + 12 * ["experiment"]

    if len(list_trial_type) == len(data_loaded_sub_part):
        data_loaded_sub_part['trial_type'] = list_trial_type
    else:
        print(['There is a mismatch between the list',
              'and the number of rows in the DataFrame for trial_type.'])
        print('The list has %s entries and the DataFrame %s. Please check both and try again' %(str(len(list_trial_cat)), str(len(data_loaded_sub)))) 

    print('Adding trial column.')

    list_stim_cat = 18 * ["shapes"] + 18 * ["images"]

    if len(list_stim_cat) == len(data_loaded_sub_part):
        data_loaded_sub_part['trial'] = list_stim_cat
    else:
        print('There is a mismatch between the list and the number of rows in the DataFrame.')
        print('The list has %s entries and the DataFrame %s. Please check both and try again' %(str(len(list_trial_cat)), str(len(data_loaded_sub)))) 

    print('Renaming columns.')

    data_loaded_sub_part.rename(
        columns={'TargetImage': 'stim_file',
                 'keyboard_response.corr': 'response',
                 'trialRespTimes': 'response_time'}, inplace=True)

    part = data_loaded_sub_part['participant_id'][0]
    ses = data_loaded_sub_part['session'][0]

    if part < 10:
        part = f"0{part}"
    else:
        part = str(part)
    
    data_dir = "/Insert/Path/On/Your/Machine/Here"
    participant_dir = os.path.join(data_dir, f"sub-{part}", f"ses-{ses}", "beh")
    
    os.makedirs(participant_dir, exist_ok=True)
    
    file_name = f"sub-{part}_ses-{ses}_task-choiceRTT_beh.tsv"
    file_path = os.path.join(participant_dir, file_name)

    print('Saving DataFrame to %s' %file_path)
    
    data_loaded_sub_part.to_csv(file_path, sep='\t', index=False)
    

# Block 3 - Data concatenation
list_dataframes = glob('/Insert/Path/On/Your/Machine/Here')

list_dataframes.sort()

list_dataframes_loaded = []

for dataframe in list_dataframes:
    
    print('Loading dataframe %s' % dataframe)
    
    df_part = pd.read_csv(dataframe, sep='\t')
    
    list_dataframes_loaded.append(df_part)

dataframe_concat = pd.concat(list_dataframes_loaded)
dataframe_concat.reset_index(drop=True, inplace=True)

dataframe_concat.to_csv('/Insert/Path/On/Your/Machine/Here',
                        sep='\t', index=False)


# Block 4 - Data analyzes


df_all_part = pd.read_csv('group_task-choiceRTT_beh.tsv', sep='\t')

df_images = []
df_images_test = df_images[df_images['session'] == 'test']
df_images_post = df_images[df_images['session'] == 'post']

df_shapes = []
df_shapes_test = df_shapes[df_shapes['session'] == 'test']
df_shapes_post = df_shapes[df_shapes['session'] == 'post']

print(df_images_test['response_time'].mean(),
      df_images_test['response_time'].std())

acc_means_img_post = [df_images_post[df_images_post['participant_id']==part]['response'].to_numpy().mean() for part in df_images_post['participant_id'].unique()]

age_list = []
pg.corr(age_list, acc_means_img_post)

pg.normality(df_shapes_test['response_time'])

pg.normality(df_shapes_post['response_time'])

pg.homoscedasticity([df_shapes_test['response_time'].to_numpy(),
                     df_shapes_post['response_time'].to_numpy()])

pg.ttest(df_shapes_test['response_time'].to_numpy(),
         df_shapes_post['response_time'].to_numpy(), paired=True)

pg.rm_anova(data=df_all_part, dv='response_time', within=['trial', 'session'], subject='participant_id', detailed=True)

smf = []
md = smf.ols("response_time ~ trial",
             df_all_part[df_all_part['session'] == 'post']).fit()

md.fvalue

md.resid
