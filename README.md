Author: Theresa Brand

### General Desctiption

Project created to run a choice reaction time task.

Reaction times have been studied for over 100 years and a staple of scientific study in psychology.
Therefore it's a perfect test case to set up an example project.

### Task Description

In a choice reaction time task, participants must make a decision related to the stimulus (such as what shape it is) and respond accordingly.
E.g., participants must click left if the stimulus is blue and right if the stimulus is red.
In this case, participants must click or press c if the stimulus is a cross, v for a square, or b for a plus.
This demo experiment has 9 practice trials and 18 experimental trials. After each practice trial, a window will appear displaying several pieces of information (to demonstrate how you could make use of responses in later routines):

- The full duration of the trial
- The RT that will appear in the datafile
- The type of response that was detected
- Whether or not the response was correct

Participants can respond in any of three ways:

- Using the appropriate keyboard keys
-Clicking on the appropriate button with the mouse
- Pressing on the appropriate button, if using a touchscreen


contact information: Theresa Brand (theresa.brand@sport.uni-giessen.de)
